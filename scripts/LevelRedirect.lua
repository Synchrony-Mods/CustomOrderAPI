local levelRedirect = {}

local debugConsole = require "SynchronyExtendedAPI.utils.DebugConsole"
local event = require "necro.event.Event"

local dungeonXMLLoader = require "necro.game.data.DungeonXMLLoader"

local eventIndex = 0

customCurrentLevelName = 1

local function getLevelTable(customLevelPath, name)
    return {
        id = customLevelPath,
        name = name or (type(customLevelPath) == "number" and customLevelPath or "custom")
    }
end

function levelRedirect.setLevelTransition(levelNumber, customLevelPath, sequence, name)
    eventIndex = eventIndex + 1
    event.levelGenerate.add(
        "registerSetLevelTransition" .. eventIndex,
        {
            sequence = sequence,
        },
        function(ev)
            if not ev.customLevelOrder then
                ev.customLevelOrder = {}
                for i = 1, 21, 1 do
                    table.insert(ev.customLevelOrder, i, { id = i, name = i })
                end
            end
            ev.customLevelOrder[levelNumber] = getLevelTable(customLevelPath, name)
        end)
end

function levelRedirect.addLevelTransition(levelNumber, customLevelPath, sequence, name)
    eventIndex = eventIndex + 1
    event.levelGenerate.add(
        "registerAddLevelTransition" .. eventIndex,
        {
            sequence = sequence,
        },
        function(ev)
            if not ev.customLevelOrder then
                ev.customLevelOrder = {}
                for i = 1, 21, 1 do
                    table.insert(ev.customLevelOrder, i, { id = i, name = i })
                end
            end
            table.insert(ev.customLevelOrder, levelNumber, getLevelTable(customLevelPath, name))
        end)
end

event.levelGenerate.override("generateProceduralLevel", {sequence = 1}, function (func, ev)
    if ev.customLevelOrder then
        local customLevel = ev.customLevelOrder[ev.level]
        customCurrentLevelName = customLevel.name

        if type(customLevel.id) == "number" then
            ev.level = customLevel.id
            func(ev)
        elseif type(customLevel.id) == "string" then
            ev.callback(dungeonXMLLoader.loadFromXMLFile(customLevel.id).levels[1])
        else
            debugConsole.printError("Invalid level: " .. customLevel.id)
        end
    else
        customCurrentLevelName = ev.level
        func(ev)
    end
end)

-- Overrride level counter

local hud = require "necro.render.hud.HUD"
local hudLayout = require "necro.render.hud.HUDLayout"
local levelTransition = require "necro.client.LevelTransition"
local netplay = require "necro.network.Netplay"
local resources = require "necro.client.Resources"
local ui = require "necro.render.UI"

local function drawLevelCounterWithDepth(depth, level, boss)
	hud.drawText {
		text = string.format("Depth: %d  Level: %s", depth, boss and "Boss" or level),
		font = ui.Font.SMALL,
		element = hudLayout.Element.LEVEL,
		alignX = 1,
		alignY = 1,
	}
end

local function drawLevelCounterWithName(name)
	hud.drawText {
		text = string.format("Custom: %s", name),
		font = ui.Font.SMALL,
		element = hudLayout.Element.LEVEL,
		alignX = 1,
		alignY = 1,
	}
end

event.renderGlobalHUD.override("renderLevelCounter", { order = "levelCounter", sequence = 1 }, function ()
	local dungeon = resources.getData(netplay.Resource.DUNGEON)

	-- Only show level counter for procedural runs
	if type(customCurrentLevelName) == "number" and dungeon and dungeon.generator and levelTransition.getCurrentLevel() then
		local levelData = dungeon.levels and dungeon.levels[customCurrentLevelName] or {}
		local levelIndex = customCurrentLevelName - 1
        drawLevelCounterWithDepth(math.floor(levelIndex / 4) + 1, levelIndex % 4 + 1, levelData.boss)
    else
        drawLevelCounterWithName(customCurrentLevelName)
	end
end)

return levelRedirect